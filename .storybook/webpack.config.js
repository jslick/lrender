module.exports = async function({ config }) {
    config.devtool = 'inline-source-map'

    const jsRule = getRule(config, 'filename.js')
    const babelLoader = getLoader(jsRule, 'babel-loader')
    // Remove polyfill configuration (makes it easier to debug)
    const envPreset = getBabelPreset(babelLoader, preset => preset.includes('@babel/preset-env/'))
    envPreset.length = 1

    return config;
}

function getRule(webpackConfig, test) {
    const rules = webpackConfig.module.rules
    for (const rule of rules) {
        if (rule.test.exec(test))
            return rule
    }
}

function getLoader(webpackRule, name) {
    const loaders = webpackRule.use
    for (const loader of loaders) {
        if (loader.loader === name)
            return loader
    }
}

function getBabelPreset(babelLoader, criteria) {
    for (const preset of babelLoader.options.presets) {
        if (criteria(preset[0]))
            return preset
    }
}
