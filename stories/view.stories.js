import { storiesOf } from '@storybook/html'

import d from '../src/util/dom-builder'
import { withStyles } from '../src/util/dom-with'
import View from '../src/view'
import Renderer from '../src/renderer'
import { template as ltemplate } from '../src/lml/template'

import { proxyArray } from './util/array-proxy'

const template = function(...args) {
    const start = new Date().getTime()
    console.log('starting parse')

    const rv = ltemplate.call(this, ...args)
    const diff = new Date().getTime() - start
    console.log('ended parse', diff)
    return rv
}


class LoadingView extends View {
    render() {
        return d('div').setText('Loading...').element
    }
}

class TaskService {
    async loadItems() {
        await wait(700)
        return this.items = proxyArray([ 'Do laundry', 'Do dishes' ])
    }

    addItem(value) {
        this.items.push(value)
    }
}

storiesOf('View', module)
    .add('TaskList', () => {
        const container = d('div').setText('...').element

        class TaskList extends View {
            constructor(props) {
                super(props)

                const { items } = this.props
                items.onChange(this._updateItemList.bind(this))
            }

            _updateItemList(index) {
                const item = this.props.items[index]

                const li = this.$node.querySelectorAll('li')[index]
                if (li) {
                    li.textContent = item
                }
                else {
                    this.$node.appendChild(d('li').setText(item).element)
                }
            }

            render() {
                const { items } = this.props

                return d('ul')
                    .append(items.map(item => d('li').setText(item).element))
                    .element
            }
        }

        class AddItemView extends View {
            eventListeners = {
                //'input: input[data-bind="item-name"]': this.handleInput,
                'input: $name': this.handleInput,
                //'click: input[data-bind="add-btn"]': this.handleAdd,
                'click: $add-btn': this.handleAdd,
                'keyup: $name': this.handleKeyup,
            }

            handleInput(evt) {
                const enabled = !!this.$refs.name.value
                this.$refs['add-btn'].disabled = !enabled
            }

            handleAdd(evt) {
                const { addItem } = this.props
                if (addItem)
                    addItem(this.$refs.name.value)
                this.reset()
                this.$refs['name'].focus()
            }

            handleKeyup(evt) {
                if (evt.key === 'Enter')
                    return this.handleAdd(evt)
            }

            reset() {
                this.$refs['name'].value = ''
                this.$refs['add-btn'].disabled = true
            }

            render() {
                return template(`
                    <label>
                        <Text value="New item:" />
                        <input :id="name" type="text" />
                    </label>
                    <input :id="add-btn" type="button" value="Add Item" />
                `).render()
                    .send(this.receiveRefs())
                    //.send('add-btn', btn => btn.disabled = true)
                    .send(this.reset.bind(this))
                    .createFragment()
            }
        }

        class TaskApp extends View {
            eventListeners = {
                'click: $refresh-btn': this.refresh,
            }

            async loadData() {
                const items = await this.props.taskService.loadItems()
                this.setState({ items })
                await wait(1000)
                items.push('Clean kitchen')
            }

            onStateChange(changedKeys) {
                if (changedKeys.includes('items'))
                    this.rerender()
            }

            addItem(value) {
                this.props.taskService.addItem(value)
            }

            refresh() {
                this.setState({ items: undefined })
                this.loadData()
            }

            onRender() {
                // console.log('$refs', this.$refs)
            }

            render() {
                if (this.state.items) {
                    return template(`
                        <Flex :id=$id :flex-direction="column" :filter="inlineFlex" :test-var=-42 :test-bool-var=true>
                            <div style="text-align:center">
                                <input :id="refresh-btn" type="button" value="Refresh" />
                            </div>
                            
                            <Flex>
                                <div>
                                    <TaskList />
                                </div>
                                <div>
                                    <AddItemView />
                                </div>
                            </Flex>
                        </Flex>
                    `).render({
                        id:             'top',
                        Flex:           renderFlex,
                        inlineFlex:     withStyles({ display: 'inline-flex' }),
                        TaskList:       this.renderChildView(TaskList, { items: this.state.items }),
                        AddItemView:    this.renderChildView(AddItemView, {
                            items:   this.state.items,
                            addItem: this.addItem.bind(this),
                        }),
                    }).send(this.receiveRefs()).get('top')
                }
                else {
                    this.loadData()

                    return this.renderChildView(LoadingView)
                }
            }
        }

        new Renderer().render(container, TaskApp, { taskService: new TaskService() })

        return container
    })

function wait(ms = 0) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}

function renderFlex(props = {}) {
    const flexDirection = props[':flex-direction'] || ''
    return d('div')
        .setStyle('display', 'flex')
        .setStyle('flex-direction', flexDirection)
        .element
}
