class ArrayProxyHandler {
    _listeners = []

    onChange(listener) {
        this._listeners.push(listener)
        return this
    }

    set(arr, prop, newVal) {
        arr[ prop ] = newVal
        const index = parseInt(prop)
        if (index || index === 0) {
            window.requestAnimationFrame(this._fireOnChange.bind(this, index))
        }
        return true
    }

    _fireOnChange(index) {
        for (const listener of this._listeners) {
            listener(index)
        }
    }
}

export
function proxyArray(array) {
    const handler = new ArrayProxyHandler()
    const proxy = new Proxy(array, handler)
    proxy.onChange = handler.onChange.bind(handler)
    return proxy
}
