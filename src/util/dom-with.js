export
function withStyles(styles) {
    return container => {
        for (const styleName in styles) {
            container.style[ styleName ] = styles[ styleName ]
        }
    }
}
