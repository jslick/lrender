/**
 * Convenience helper for manipulating DOM elements
 */
class DomBuilder {
    /**
     * @param {string} tagName  Creates a new element using this tag name
     */
    constructor(tagName) {
        /**
         * @member {HTMLElement}
         * @readonly
         *
         * Element being manipulated
         */
        this.element = document.createElement(tagName)
    }

    /**
     * Adds a class name
     * @param {string} className
     * @returns {DomBuilder} self
     */
    addClass(className) {
        this.element.classList.add(className)
        return this
    }

    /**
     * Sets the element textContent
     * @param {string} text
     * @returns {DomBuilder} self
     */
    setText(text) {
        this.element.textContent = text
        return this
    }

    /**
     * Sets the element innerHTML
     * @param {string} html
     * @returns {DomBuilder} self
     */
    setHtml(html) {
        this.element.innerHTML = html
        return this
    }

    setAttribute(attName, attValue) {
        this.element.setAttribute(attName, attValue)
        return this
    }

    /**
     * Sets a dataset property
     * @param {string} dataName     Data name, in camel-case
     * @param {string} dataValue
     * @returns {DomBuilder} self
     */
    setData(dataName, dataValue) {
        this.element.dataset[dataName] = dataValue
        return this
    }

    /**
     * Sets an element style
     * @param {string} styleName
     * @param {string} value
     * @returns {DomBuilder} self
     */
    setStyle(styleName, value) {
        this.element.style[styleName] = value
        return this
    }

    /**
     * Appends child nodes to the element
     * @param {... Node} nodes
     * @return {DomBuilder} self
     */
    append(nodes) {
        this.element.append(...nodes)
        return this
    }
}

/**
 * Shortcut for creating a DomBuilder
 * @param {string} tagName  Tag name of element to create
 * @returns {DomBuilder}
 */
function d(tagName) {
    return new DomBuilder(tagName)
}

export default d
