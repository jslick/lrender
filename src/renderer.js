import View from './view'

/**
 * @callback renderCallback
 * @return Node
 */

/**
 * @callback renderChildCallback
 * @param {View} view
 * @param {object} props
 * @return Node
 */

/**
 * @typedef {object} ViewContext
 * @property {renderCallback} rerenderSelf
 * @property {renderChildCallback} renderChildView
 */

export default
class Renderer {
    render(containerElement, ViewClass, props = {}) {
        const instance = this._instantiateView(ViewClass, props)
        instance.onRender = wrapFunction(instance.onRender, rendered => {
            return replaceDomContent(containerElement, rendered)
        })
        return this._renderViewInstance(instance)
    }

    _instantiateView(ViewClass, props = {}) {
        const instance = new ViewClass(props)
        instance.$viewContext = {
            rerenderSelf: this._renderViewInstance.bind(this, instance),
            renderChildView: this._renderView.bind(this),
        }
        //instance._setRenderer(this)
        return instance
    }

    _renderView(ViewClass, props = {}) {
        const instance = this._instantiateView(ViewClass, props)
        return this._renderViewInstance(instance)
    }

    _renderViewInstance(viewInstance) {
        const rendered = viewInstance.render()
        viewInstance.$rendered = rendered
        const renderedNode = this._render(rendered)
        viewInstance.onRender(renderedNode)
        viewInstance.$node = renderedNode
        this._bindEventListeners(viewInstance)
        return renderedNode
    }

    _render(what) {
        if (what instanceof View) {
            return this._renderView(what)
        }
        else if (what instanceof Node) {
            return what
        }
        else if (typeof what === 'string') {
            return document.createTextNode(what)
        }
        else {
            throw new Error('Unknown `what`')
        }
    }

    _bindEventListeners(viewInstance) {
        if (!viewInstance.eventListeners)
            return

        for (const listenerDef in viewInstance.eventListeners) {
            const match = /^(\w+):\s*(.*)$/.exec(listenerDef)
            if (!match)
                throw new Error('Bad event listener definition')

            const [ _, eventName, selector ] = match
            const method = viewInstance.eventListeners[listenerDef]

            const elements = selector[0] === '$' ?
                [ viewInstance.$refs[selector.substring(1)] ] :
                viewInstance.$node.querySelectorAll(selector)
            Array.from(elements)
                .filter(Boolean)
                .map(element => {
                    element.addEventListener(eventName, method.bind(viewInstance))
                })
        }
    }
}

function wrapFunction(origFunction, wrapFunction) {
    return function(...args) {
        wrapFunction(...args)
        return origFunction.apply(this, args)
    }
}

function replaceDomContent(container, renderedContent) {
    container.textContent = ''
    if (Array.isArray(renderedContent)) {
        container.append(...renderedContent)
    }
    else {
        container.appendChild(renderedContent)
    }
}
