import { parseLml } from './parser'
import { renderElement, renderFragment, renderText } from './views'

export
class TemplateError extends Error {
    constructor(message) {
        super(message)

        this.name = this.constructor.name
        this.stack = (new Error(message)).stack
    }
}

export
class Template {
    constructor(nodes, defaultVars = {}) {
        this._nodes = nodes
        this._vars = { ...defaultVars }

        this._nodesById = {}
    }

    render(vars) {
        const allVars = { ...this._vars, ...vars }

        this._renderedNodes = this._nodes.map(node => this._renderNode(node, allVars))

        return this
    }

    get(nodeId) {
        return this._nodesById[nodeId]
    }

    getRoots() {
        return this._renderedNodes.slice()
    }

    send(...args) {
        if (args.length === 1) {
            const receiver = args[0]
            receiver(Object.assign({}, this._nodesById))
        }
        else if (args.length === 2) {
            const [ id, callback ] = args
            callback(this._nodesById[id])
        }

        return this
    }

    createFragment() {
        const frag = document.createDocumentFragment()
        frag.append(...this._renderedNodes)
        return frag
    }

    _renderNode(node, vars) {
        const nodeTemplate = vars[node.tagName]
        if (!nodeTemplate)
            throw new TemplateError(`${ node.tagName } is not bound`)

        let container = invokeRenderCallback(nodeTemplate, node, vars)

        if (node.props[':filter']) {
            const keyname = resolveReference(node.props[':filter'], vars)
            // const filter = vars[node.props[':filter']]
            const filter = vars[keyname]
            if (filter)
                container = invokeNodeFilter(filter, container, node, vars)
        }

        // const nodeId = node.props[':id']
        const nodeId = resolveReference(node.props[':id'], vars)
        if (nodeId)
            this._nodesById[nodeId] = container

        if (node.children.length) {
            container.textContent = ''
            const children = node.children.map(childNode => this._renderNode(childNode, vars))
            container.append(...children)
        }
        return container
    }
}

function getDefaultBindings() {
    return {
        div: renderElement.bind(undefined, 'div'),
        label: renderElement.bind(undefined, 'label'),
        input: renderElement.bind(undefined, 'input'),
        button: renderElement.bind(undefined, 'button'),

        Fragment: renderFragment,
        Text: renderText,
    }
}

export
function template(markup) {
    const nodes = parseLml(markup)

    return new Template(nodes, getDefaultBindings())
}

// Helpers //

function invokeRenderCallback(nodeTemplate, node, vars) {
    if (typeof nodeTemplate === 'function') {
        // return nodeTemplate(node.props)
        return nodeTemplate(resolveProps(node.props, vars))
    }
    else if (nodeTemplate instanceof Node) {
        return nodeTemplate
    }
    else {
        throw new TemplateError('Invalid template variable')
    }
}

function invokeNodeFilter(nodeTemplate, container, node, vars) {
    // const filteredContainer = nodeTemplate(container, node.props)
    const filteredContainer = nodeTemplate(container, resolveProps(node.props, vars))
    // Template returning null will cause a filter to null
    return typeof filteredContainer === 'undefined' ? container : filteredContainer
}

function resolveReference(value, vars) {
    // NOTE: Only doing 1-level resolve for performance
    return value && value.referenceName ? vars[value.referenceName] : value
}

function resolveProps(props, vars) {
    return Object.keys(props).reduce((resolved,name) => {
        const keyname = resolveReference(name, vars)
        resolved[keyname] = props[keyname]
        return resolved
    }, {})
}
