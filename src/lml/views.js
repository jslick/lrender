function filterPropsToAttrs(props = {}) {
    return Object.keys(props).reduce((attrs, propName) => {
        if (propName[0] !== ':')
            attrs[propName] = props[propName]
        return attrs
    }, {})
}

export
function renderElement(tagName, props = {}) {
    const element = document.createElement(tagName)
    const attrs = filterPropsToAttrs(props)
    for (const attrName of Object.keys(attrs)) {
        element.setAttribute(attrName, attrs[attrName])
    }
    return element
}

export
function renderFragment(props = {}) {
    return document.createDocumentFragment()
}

export
function renderText(props = {}) {
    return document.createTextNode(props.value || '')
}
