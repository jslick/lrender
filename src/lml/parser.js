// Patterns
const IDENTIFIER = /^[a-zA-Z:](?:\w|[:\-])*/
const REFERENCE = new RegExp('^\\$(' + IDENTIFIER.source.substring(1) + ')')
const DECIMAL_INT = /^-?\d+/

class Token {
    constructor(name, value) {
        this.name = name
        this.value = value
    }

    toString() {
        return [
            `Token ${ this.name }`,
            this.value ? `: "${ this.value }"` : false
        ].filter(Boolean).join('')
    }
}

function tokenize(markup) {
    const tokens = []

    let currentLine = 1

    for (let i = 0; i < markup.length;) {
        switch (markup[i]) {
        case '<':
        case '>':
        case '/':
        case '=':
            tokens.push(new Token(markup[i]))
            i++
            continue

        case '"': {
            const match = /^"([^"]*)"/.exec(markup.substring(i))
            tokens.push(new Token('string', match[1]))
            i += match[0].length
            continue
        }
        }

        let match = null
        if (match = /^true|false/.exec(markup.substring(i))) {
            tokens.push(new Token('boolean', match[0] === 'true'))
            i += match[0].length
            continue
        }
        if (match = IDENTIFIER.exec(markup.substring(i))) {
            tokens.push(new Token('identifier', match[0]))
            i += match[0].length
            continue
        }
        else if (match = REFERENCE.exec(markup.substring(i))) {
            tokens.push(new Token('reference', match[1]))
            i += match[0].length
            continue
        }
        else if (match = DECIMAL_INT.exec(markup.substring(i))) {
            const decimal = parseInt(match[0])
            if (Number.isNaN(decimal))
                throw new Error(`Internal parse error at line ${ currentLine }, parsing number '${ match[0] }'`)
            tokens.push(new Token('decimal', decimal))
            i += match[0].length
            continue
        }
        else if (match = /^\s+/.exec(markup.substring(i))) {
            i += match[0].length
            const newlineMatch = match[0].match(/\n/g)
            if (newlineMatch)
                currentLine += newlineMatch.length
            continue
        }
        else {
            throw new Error(`Token error at line ${ currentLine }, character '${ markup[i] }'`)
        }
    }

    return tokens
}

class Node {
    children = []

    append(node) {
        this.children.push(node)
    }
}

class TagNode extends Node {
    constructor(tagName) {
        super()
        this.tagName = tagName
        this.props = {}
    }
}

class Reference {
    constructor(name) {
        this.referenceName = name
    }
}

class ParseContext {
    parents = []

    pushParent(node) {
        this.parents.push(node)
    }

    popParent() {
        return this.parents.pop()
    }

    append(node) {
        this.parents[this.parents.length-1].append(node)
    }
}

class ParseError extends Error {
    constructor(msg) {
        super(msg)
    }
}

class Parser {
    constructor(tokens) {
        this.tokens = tokens
        this.curIndex = 0
    }

    cur() {
        return this.tokens[this.curIndex]
    }

    peek(tokenName) {
        const cur = this.cur()
        return cur.name === tokenName ? cur : false
    }

    accept(tokenName) {
        const cur = this.cur()
        if (cur.name === tokenName) {
            this.curIndex++
            return cur
        }
        else {
            return false
        }
    }

    expect(tokenName) {
        const token = this.accept(tokenName)
        if (token)
            return token
        else
            throw new ParseError(`Unexpected token: Expected "${ tokenName }" token, but received ${ this.cur() }`)
    }

    parse() {
        const nodes = []
        const context = new ParseContext()
        context.pushParent({
            append(node) {
                nodes.push(node)
            }
        })
        this.parseTop(context)

        return nodes
    }

    parseTop(context) {
        while (this.cur())
            this.parseTag(context)
    }

    parseTag(context) {
        this.expect('<')

        if (this.peek('/'))
            return this.parseCloseTag(context)
        else
            return this.parseOpenTag(context)
    }

    parseOpenTag(context) {
        const tagNameToken = this.expect('identifier')
        const tagNode = new TagNode(tagNameToken.value)
        context.append(tagNode)

        const props = this.parseProps(context)
        tagNode.props = {
            ...tagNode.props,
            ...props,
        }

        if (this.accept('/')) {
            // Self-closing tag
            this.expect('>')
            // context.popParent()
            return tagNode
        }
        else {
            // Open-close tag
            this.expect('>')

            context.pushParent(tagNode)
            this.parseTop(context)
        }
    }

    parseCloseTag(context) {
        this.expect('/')
        const tagNameToken = this.expect('identifier')
        // TODO:  Not matching tag name yet
        context.popParent()
        this.expect('>')
    }

    parseProps(context) {
        const props = {}

        let propNameToken
        while (propNameToken = this.accept('identifier')) {
            this.expect('=')

            const value = this.parsePropValue(context)
            props[propNameToken.value] = value
        }

        return props
    }

    parsePropValue(context) {
        let token
        if (token = this.accept('string')) {
            return token.value
        }
        else if (token = this.accept('decimal')) {
            return token.value
        }
        else if (token = this.accept('boolean')) {
            return token.value
        }
        else if (token = this.accept('reference')) {
            return new Reference(token.value)
        }
        else {
            throw new ParseError(`Unexpected token: Received ${ this.cur() }`)
        }
    }
}

export
function parseLml(markup) {
    const tokens = tokenize(markup)
    // console.log('tokens', tokens)

    const nodes = new Parser(tokens).parse()
    // console.log('nodes', nodes)
    return nodes
}
