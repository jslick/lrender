export default
class View {
    constructor(props = {}, initialState = {}) {
        this.props = Object.assign({}, props)
        this.state = Object.assign({}, initialState)

        //this._View_private = {}
    }

    /**
     * @protected
     * @type {ViewContext}
     */
    $viewContext = null

    /**
     * @protected
     * @type {Node}
     */
    $node = null

    /**
     * @protected
     * @type {object}
     * Maps nodes by a locally-unique identifier <=> A rendered node for that LUID
     *
     * Note, the type of the values are not typed because it's up to the view to determine what they are and how to
     * render them.  But, many View's might put Node's in there.
     */
    $refs = {}

    $get(selector) {
        return this.$node.querySelector(selector)
    }

    $$get(selector) {
        return this.$node.querySelectorAll(selector)
    }

    receiveRefs() {
        return nodesById => {
            Object.assign(this.$refs, nodesById)
        }
    }

    rerender() {
        return this.$viewContext.rerenderSelf()
    }

    onRender() {}

    renderChildView(ViewClass, props = {}) {
        return this.$viewContext.renderChildView(ViewClass, props)
    }

    setState(newState) {
        const changedKeys = Object.keys(newState).filter(key => {
            return this.state[key] !== newState[key]
        })
        Object.assign(this.state, newState)

        this.onStateChange(changedKeys)
    }

    onStateChange(changedKeys) {}
}

//// Get private variable
//function priv(view) {
//    return view._View_private
//}
